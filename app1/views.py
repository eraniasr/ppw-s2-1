from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Erania Siti Rajisa' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 9, 29) #TODO Implement this, format (Year, Month, Date)
npm = '1706039401' # TODO Implement this
hobby = 'reading novel'
uni = 'Universitas Indonesia'
desc = 'Saya sedang mengalami krisis identitas :('

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'uni': uni, 'hobby': hobby, 'desc': desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
